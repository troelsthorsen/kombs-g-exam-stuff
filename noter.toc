\select@language {english}
\select@language {english}
\contentsline {section}{\numberline {1}P, NP and NPC}{5}
\contentsline {subsection}{\numberline {1.1}Introduction}{5}
\contentsline {subsection}{\numberline {1.2}Formal definition of P}{5}
\contentsline {subsection}{\numberline {1.3}Formal definition of NP}{5}
\contentsline {subsection}{\numberline {1.4}Reductions}{5}
\contentsline {subsection}{\numberline {1.5}Formal definition of NP-HARD}{5}
\contentsline {subsection}{\numberline {1.6}NPC}{5}
\contentsline {subsection}{\numberline {1.7}Lemma 9}{6}
\contentsline {subsection}{\numberline {1.8}SAT $\leq $ 3SAT}{7}
\contentsline {section}{\numberline {2}Cook's theorem and the complexity of variants of SAT}{8}
\contentsline {subsection}{\numberline {2.1}Boolean Circuits \& CNF SAT}{8}
\contentsline {subsection}{\numberline {2.2}Lemma 8}{8}
\contentsline {subsection}{\numberline {2.3}Lemma 9}{8}
\contentsline {subsection}{\numberline {2.4}Cook's theorem}{8}
\contentsline {subsubsection}{\numberline {2.4.1}Circuit SAT is NPC}{8}
\contentsline {subsubsection}{\numberline {2.4.2}Circuit SAT $\leq $ SAT}{9}
\contentsline {subsubsection}{\numberline {2.4.3}Example of getting the variables right:}{10}
\contentsline {subsection}{\numberline {2.5}Other Variants of SAT}{10}
\contentsline {subsubsection}{\numberline {2.5.1}2SAT $\in $ P}{10}
\contentsline {subsubsection}{\numberline {2.5.2}NAESAT $\in $ NPC}{10}
\contentsline {subsubsection}{\numberline {2.5.3}MAX2SAT $\in $ NPC}{10}
\contentsline {subsubsection}{\numberline {2.5.4}3SAT $\in $ NPC}{10}
\contentsline {section}{\numberline {3}NP-complete graph problems}{11}
\contentsline {subsection}{\numberline {3.1}Independent Set Definition}{11}
\contentsline {subsection}{\numberline {3.2}3SAT $\leq $ INDEPENDENT SET}{11}
\contentsline {subsection}{\numberline {3.3}Hamilton Path Definition}{11}
\contentsline {subsection}{\numberline {3.4}3SAT $\leq $ HAMILTON PATH}{12}
\contentsline {subsection}{\numberline {3.5}HAMILTON PATH $\leq $ TSP}{12}
\contentsline {subsection}{\numberline {3.6}Other graph problems}{13}
\contentsline {subsubsection}{\numberline {3.6.1}Clique}{13}
\contentsline {subsubsection}{\numberline {3.6.2}Node Cover}{13}
\contentsline {section}{\numberline {4}NP-complete problems w. sets and numbers}{14}
\contentsline {subsection}{\numberline {4.1}3SAT $\leq $ TRIPARTITE MATCHING}{14}
\contentsline {subsection}{\numberline {4.2}EXACT COVER BY 3-SETS}{15}
\contentsline {subsection}{\numberline {4.3}TRIPARTITE MATCHING $\leq $ EXACT COVER BY 3-SETS}{15}
\contentsline {subsection}{\numberline {4.4}KNAPSACK}{16}
\contentsline {subsection}{\numberline {4.5}EXACT COVER BY 3-SETS $\leq $ KNAPSACK}{16}
\contentsline {subsection}{\numberline {4.6}SET COVERING}{16}
\contentsline {subsection}{\numberline {4.7}SET PACKING}{16}
\contentsline {section}{\numberline {5}Approximation algorithms}{17}
\contentsline {subsection}{\numberline {5.1}Optimal Vertex-cover problem}{17}
\contentsline {subsection}{\numberline {5.2}Approximation to Vertex-Cover}{17}
\contentsline {subsection}{\numberline {5.3}Vertex-Cover approximation algo is 2-approx}{17}
\contentsline {subsection}{\numberline {5.4}The General Design/Analysis Trick}{18}
\contentsline {subsection}{\numberline {5.5}Travelling Salesman Problem}{18}
\contentsline {subsubsection}{\numberline {5.5.1}The Triangle Inequality}{18}
\contentsline {subsection}{\numberline {5.6}Approx-TSP-Tour}{18}
\contentsline {subsection}{\numberline {5.7}Is it 2-approx?}{19}
