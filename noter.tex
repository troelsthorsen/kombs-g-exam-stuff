\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{MnSymbol}
\usepackage{fancyref}
\usepackage{cases}
\usepackage{enumitem}
\usepackage{listings}

\author{Troels Thorsen}
\title{\huge Combinatorial Search\\ \vspace{1cm} Exam Notes \vspace{5cm} }



\begin{document}
%% line spacing in enumerate
\setlist[enumerate]{itemsep=0mm}
\maketitle
\newpage
\tableofcontents
\newpage
\section*{Exam Curriculum}
\begin{itemize}
\item Peter Bro Miltersen, P, NP and NPC
\item Christos H. Papadimitriou, Computational Complexity, pages 181-206.
\item T.H. Cormen, C.E. Leiserson, R. Rivest, C. Stein, Introduction to Algorithms second edition, pages 1022-1043, except 1033-1038.
\item Kristoffer Arnsfelt Hansen, Approximation algorithms: Supplementary notes for Set Cover and Knapsack.
\item D.S. Johnson and L.A. McGeoch, The traveling salesman problem: a case story, pages 215-241, 246-273, 286-305, 309-310.
\item Slides presented at lectures May 28 and May 30
\end{itemize}

\section*{Overview of problems in NPC}
\begin{center}
\includegraphics[width=\textwidth]{overview_npc}
\end{center}

\newpage

\section{P, NP and NPC}

\subsection{Introduction}
\begin{itemize}
	\item 	What is a \textbf{decision problem}? \\
			$ \{0,1\}^\star \rightarrow \{yes,no\} $
	\item 	What is a \textbf{language}? \\
			$ L \subseteq \{0,1\}^\star$
	\item 	What is a \textbf{stand-in language}? \\
			Language representing a problem which is not a decision problem naturally. \\
			An Example can be the language $L_f$ of the function $f : \{0,1\}^\star \rightarrow \{0,1\}^\star$ 
			\begin{center}
			$L_f = \{ \langle x, b(j), y \rangle : f(x)_j = y \}$\\ 
			Where $x \in  \{0,1\}^\star, j \in \mathbb{N}, y \in \{0,1\}$
			\end{center}
\end{itemize}

\subsection{Formal definition of P}
\begin{equation} 
	P = \{ L \subseteq \{0,1\}^\star : \exists \text{$TM$ $M_l$ that decides $L$ in polynomial time}\}
\end{equation}

\subsection{Formal definition of NP}
The following decides if a language L $\in$ NP:
\begin{equation}
	\forall x : x \in L \iff [\exists y \in \{0,1\}^\star : |y| \leq p(|x|) \wedge \langle x,y \rangle \in L' ]
\end{equation}

\subsection{Reductions}
A reduction is a mapping from one language to some other language.\\
The reduction is a \textbf{polynomial time computable map} between the languages. \\
We write a reduction from $L_1$ to $L_2$ as follows: $L_1 \leq L_2$.\\
Formally: 
\begin{equation}
	\forall x : x \in L_1 \iff r(x) \in L_2 
\end{equation}
Where $r(x)$ is the map.
\textbf{Transitivity }between reductions can be proven by using multiple maps inside eachother.

\subsection{Formal definition of NP-HARD}
\begin{equation}
	\forall L' \in NP : L' \leq L
\end{equation}

\subsection{NPC}
Those languages that are in NPC, are both in NPH and NP.
% Draw this. 

\subsection{Lemma 9}
Given a TM M, with input length n, we are able to verify a language $\in P$. 
We know $p(n) \geq n$. Motivation is that we can construct a Circuit of size $O(p(n)^2)$.
This circuit we will call $C_n$, and the following holds: $C_n(x) = 1 \iff \text{M accepts x}$.

We construct the turing machine, where the upper cells are +1 ahead in time:
\begin{center}
\includegraphics[width=0.8\textwidth]{lemma9_1}
\end{center}

and we define the cell-state-vector $c_{t,i}$, where \emph{t} is time, and \emph{i} is index:
\begin{equation}
	c_{t,i} = 
		\begin{cases}
			\text{What does the tape on index i say?}\\
			\text{Is the tape-head here?} \\
			\text{Where in the finite control are we?}
		\end{cases} 
\end{equation}

By the drawing, we define the cell-state-vector as: 
\begin{equation}
	c_{t,i} = h(c_{t-1,i-1},c_{t-1,i}, c_{t-1,i+1})
\end{equation}
And the function h:
\begin{equation}
	h : \{0,1\}^{3s} \rightarrow \{0,1\}^{s} 
\end{equation}
From lemma 8, we know that a boolean function can be translated to a Circuit.
S is the size of the bit-string it takes to represent the cell-state-vector. \\
We have now defined a block D, which takes 3S as an input and outputs S, corresponding to the function h.
\begin{center}
\includegraphics[width=0.4\textwidth]{lemma9_2}
\end{center}

Now we can begin to construct the amount of steps it will take to do what we want.
The B blocks will be blanks, only giving output as "\#".
In the final, top level, we will only have a single output from our D blocks, into the F blocks.
The F blocks are defined as follows:
\begin{equation}
	F = \begin{cases}
			1 & \text{if Tape-head is here and we are in an accept state} \\
			0 & \text{else}
		\end{cases}
\end{equation}
This results in the following construction.
\begin{center}
\includegraphics[width=0.8\textwidth]{lemma9_3}
\end{center}
We take at most $2p(n)p(n) = O(p(n)^2)$ steps to do the above. This is defined by the amount of D blocks created. 

\subsection{SAT $\leq$ 3SAT}
We are interested in the following clauses:
\begin{equation}
	\begin{array}{llll}
		\text{1 literal}	&	(x_1)			&	\Rightarrow	&	
			(x_1 \vee u_1 \vee u_2) \wedge
			(x_1 \vee \neg u_1 \vee u_2) \wedge
			(x_1 \vee u_1 \vee \neg u_2) \wedge
			(x_1 \vee \neg u_1 \vee \neg u_2) \\
			
		\text{2 literals}	&	(x_1 \vee x_2)	&	\Rightarrow	&	
			(x_1 \vee x_2 \vee u_1) \wedge
			(x_1 \vee x_2 \vee \neg u_1) \\
			
		\text{3 literals}	&	(x_1 \vee x_2 \vee x_3) & \Rightarrow & 
			(x_1 \vee x_2 \vee x_3) \\
			
		> \text{3 literals} &	(x_1 \vee \dots \vee x_n) & \Rightarrow &
			(x_1 \vee x_2 \vee u_1) \wedge 
			\\ &&&
			(x_3 \vee \neg u_1 \vee u_2) 
			 \wedge \dots \wedge
			(x_{i} \vee \neg u_{i-2} \vee u_{i-1}) 
			 \wedge \dots \wedge 
			\\ &&&
			(x_{n-1} \vee \neg u_{n-2} \vee x_n)
	\end{array}
\end{equation}

1 literal takes $O(1)$, 2 literals take $O(1)$, 3 literals take $O(1)$. \\
Clauses with more than 3 literals will take $O(\#literals$ to construct. \\
We can finally show correctness using truth tables, to show the expressions 
are the same in terms of expressive power.


\newpage

\section{Cook's theorem and the complexity of variants of SAT}
\subsection{Boolean Circuits \& CNF SAT}
Give an example of a satisfying and not satisfying.
Explain what a boolean circuit is, and then a cnf.

\subsection{Lemma 8}
\label{sec:lemma8}
For any boolean function $f: \{0,1\}^n \rightarrow \{0,1\}^m$, there is a circuit \emph{C},
so that $\forall x \in \{0,1\}^n : C(x) = f(x)$

\subsection{Lemma 9}
Let any Turing Machine \emph{M} running in time at most $p(n) \geq n$,
on inputs of length \emph{n}, where \emph{p} is a polynomial, be given.
\\
Then, given any fixed input length \emph{n}, there is a circuit $C_n$ if size at most
$O(p(n)^2)$ so that $\forall x : x \in \{0,1\}^n | C_n(x) = 1 \iff M \text{ accepts } x.$
\\
Furthermore, the function mapping $1^n$ to a description of $C_n$ is polynomial time computable. 
\\


Motivation is, that we want to use Circuits to compute stuff on our computers. Since
computers are already built of circuits, it should be possible to translate a boolean
circuit to a TM. 
\subsection{Cook's theorem}
$SAT \in NPC$
\subsubsection{Circuit SAT is NPC}
We know Circuit is in NP.
We just need to show that every language in NP reduces to it, and thereby show it's NP-hard.

We simply want to show for a language $L \in NP$ that:
\begin{equation}
	\forall x : x \in L \iff r(x) \in \text{CIRCUIT SAT}
\end{equation}

We recall the definition of a language is in NP:
\begin{equation}
	\forall x : x \in L \iff [\exists y \in \{0,1\}^\star : |y| \leq p(|x|) \wedge \langle x,y \rangle \in L' ]
\end{equation}

We want the language of L to map to instances of CIRCUIT SAT. 
Thereby we want the reduction r(x) for any x, to translate to a Circuit.
This Circuit must at most have $p(|x|)$ sub-circuits, combined with OR-gates, such that:
\begin{equation}
	C = D_0 \vee D_1 \vee \dots \vee D_{p(|x|)}
\end{equation}

Each of these D's must take \emph{i} inputs, and return 1 on a given input 
\begin{equation}
	y \in \{0,1\}^i \iff \langle x,y \rangle \in L'
\end{equation}

Let's say we have a turing machine that can decide L' in polynomial time.
\\
From Lemma 9, we know that there exists a Turing Machine, that given an input length \emph{n}, has an algorithm, which can output a Circuit $C_n$, such that:
\begin{equation}
	\forall x, y : | \langle x,y \rangle | = n
\end{equation}
We have that 
\begin{equation}
	C_n( \langle x,y \rangle) = 1 \iff M \text{ accepts } \langle x,y \rangle \in L'
\end{equation}

In this case, we can let:
\begin{equation}
	n = | \langle x,y \rangle | = 2(|x| + i ) + 2  \qquad \text{ for } 0 \leq i \leq p(|x|)
\end{equation}

The circuit $C_n$ must be modified according to its input, since the pairing function looks like:
\begin{equation}
	\langle x,y \rangle = x_1 0 x_2 0 x_3 0 \dots x_{n-1} 11 y_1 0 y_2 \dots 
\end{equation}

We only want to grab the gates that expres something, namely :  
\begin{equation}
	X_1, X_3, X_5, \dots X_2|x|-1
\end{equation}

Therefore we change the gates the following:
\begin{itemize}
	\item Gates $X_{2i-1}$ (uneven gates) $\rightarrow x_i$
	\item Gate $X_{2|x|+2} \rightarrow 1$
	\item Gate $X_{2|x|+3} \rightarrow 1$
	\item The remaining are changed to 0.
\end{itemize}
Now all gates should be "hooked up" according to input, and the circuit $D_i$ is correct.

This should be done for all the circuits of D, and combined in the end with \textbf{OR} gates. The final circuits form a Circuit C.

This reduction is done in polynomial time, so that means: $CIRCUIT SAT \in NPC$

\subsubsection{Circuit SAT $\leq$ SAT}
Now we just need to show that $SAT \in NPC$ by reducting Circuit SAT.\\
Given a 1-output circuit C, we want to construct a CNF $f = r(C)$, so that \emph{f} 
has a satisfying assignment iff C has it. We want \emph{r} to be our polynomial time computable map.
\\
For each gate \emph{g} in C, we use the following rules to translate into a CNF:
\begin{equation}
\begin{array}{lcl}
	AND(h_1,h_2) 	& 	\iff 	& (\neg g \vee h_1) \wedge (\neg g \vee h_2) \wedge (g \vee \neg h_1 \vee \neg h_2)  \\
	OR(h_1,h_2)		& 	\iff 	& (g \vee \neg h_1) \wedge (g \vee \neg h_2) \wedge (\neg g \vee h_1 \vee h_2) \\
	NOT(h) 			& 	\iff 	& (g \vee h) \wedge (\neg g \vee \neg h) \\
	COPY(h)			& 	\iff 	& (g \vee \neg h) \wedge (\neg g \vee h) \\
	0	 			& 	\iff 	& (\neg g) \\
	1				& 	\iff 	& (g) \\
	OUTPUT			& 	\iff 	& (g)		
\end{array}
\end{equation}
Where the OUTPUT gate is the unqiue output gate, resembling the final result. \\
The above combinations are combined with \textbf{$\wedge$} in f.
This proves it's possible to make a polynomial time computable map.
We have now proved that CIRCUIT SAT $\leq$ SAT. 

\subsubsection{Example of getting the variables right:}
\begin{equation}
	\begin{array}{l}	
		\text{We know that } a \iff b \text{ also means } (\neg a \vee b) \wedge (a \vee \neg b) \\
		\text{The and-gate can be expressed as } g \iff (h_1 \vee h_2)\\ 
		\text{We know that } \neg (a \wedge b) \iff \neg h_1 \vee \neg h_2
	\end{array}
\end{equation}
We can use these in a combination giving us the following
\begin{equation}
	\begin{array}{ll}
		&\neg g \vee (h_1 \wedge h_2)) \wedge (\neg (h_1 \wedge h_2) \vee g) \\
		\iff &(\neg g \vee h_1) \wedge (\neg g \vee h_2) \wedge (g \vee \neg h_1 \vee \neg h_2)
	\end{array}
\end{equation}

\subsection{Other Variants of SAT}
This is only if there is time left.

\subsubsection{2SAT $\in$ P}
Solve each literal once at a time. Remove the clauses according to the ones you have satisfied. 
\subsubsection{NAESAT $\in$ NPC}
Not-all-equal SAT, where we don't allow all literals in a clause to be true or false. 
\subsubsection{MAX2SAT $\in$ NPC}
Can we satisfy at least K of these clauses in 2SAT?
\subsubsection{3SAT $\in$ NPC}
Reduction of this perhaps?

\newpage
\section{NP-complete graph problems}

\subsection{Independent Set Definition}
\begin{equation}
	\text{Given } G(V,E)\\
	I \subseteq V \text{ is independent if } (i,j) \not\in E \text{ for every } i,j \in I
\end{equation}
Decision problem:
\begin{equation}
	L_{ind.set.} = \{\langle G, K \rangle | \text{ does G have independent set of size K? } \}
\end{equation}


\subsection{3SAT $\leq$ INDEPENDENT SET}
Reduction with the Triangle Gadgets.
\begin{center}
	\includegraphics[width=\textwidth]{independent_set}
\end{center}
Correctness:
\begin{itemize}
	\item Each clause can have at max 1 true vertice. (Since it's a triangle)
	\item We MUST need at least one, in each clause to satisfy.
	\item We ensure that a literal cannot be picked as both true or false by creating an edge between those. 
\end{itemize}

\subsection{Hamilton Path Definition}
Path in a graph, where each vertex is visited exactly once.

\subsection{3SAT $\leq$ HAMILTON PATH}
Reduction with the Variable Gadget, Clause-Gadget and the Choice Gadget.
\begin{center}
	\includegraphics[width=\textwidth, angle=-90]{hamilton_path}
\end{center} 
Correctness:
\begin{itemize}
	\item Choice, all variables must have assigned a truth value.
	\item Consistency, all literals we give a truth assignment will have the same value.
	\item Constraint, there must be at least one edge in the clause gadget used.
\end{itemize}

\subsection{HAMILTON PATH $\leq$ TSP}
Language for TSP:
\begin{equation}
	L_{TSP} = \{ \langle G, K \rangle | \exists ? \text{ a HAMILTON PATH with }\sum_{(i,j) \in \text{visited E}} d_{i,j} \leq K? \}
\end{equation}
The budget is very important in this. We just want to show that it's possible to solve the
hamilton path by using the TSP problem, and not solving the TSP problem sufficiently.

Given $G=(V,E)$ where $V=\{1, \dots, n\}$\\
Make distances accordingly: 
\begin{equation}
		d_{i,j} =
		\begin{cases}
			1 & \text{if } (i,j) \in E,
			\\
			2 & \text{if } (i,j) \not\in E
			\end{cases}
\end{equation}
The budget of our TSP should then be n+1, since we have n-1 edges visited in a hamilton path.

\subsection{Other graph problems}
This is only if there is time left. 
\subsubsection{Clique}
\begin{equation}
\begin{array}{l}
	\text{Given } G(V,E) \\ 
	C \subseteq V \text{ is a clique if } (i,j) \in E \text{ for every } i,j \in C
\end{array}
\end{equation}
\subsubsection{Node Cover}
\begin{equation}
\begin{array}{l}
	\text{Given } G(V,E) \\
	N \subseteq V \text{ is a node cover if } (i,j) \in E \text{ for every } i \in N \vee j \in N
\end{array}
\end{equation}
\newpage
\section{NP-complete problems w. sets and numbers}
Motivation for this is, that we have shown \\
CIRCUIT SAT $\rightarrow$ SAT $\rightarrow$ 3SAT, and now we will show that
TRIPARTITE MATCHING also lies in NPC. \\
After we have shown TRIPARTITE MATCHING is in NPC, we can show the following.

\subsection{3SAT $\leq$ TRIPARTITE MATCHING}
We are given a formulae \emph{f} in 3SAT, with variables $x_1, \dots x_n$ and clauses \emph{m}. \\
Now we need to construct instances of TRIPARTITE MATCHING, with 
a polynomial time reducing map r.\\

\begin{center}
	\includegraphics[width=0.5\textwidth]{tripartite}
\end{center}
To do this efficiently, we use the \textbf{Choice consistency gadget}.
The recipe for destruction is as follows:
\begin{enumerate}
	\item For each variable $x_i \in f$, construct at choice consistency gadget.
		\begin{enumerate}
			\item Let $k = max(\#x_i,\#\neg x_i)$
			\item Construct a gadget with \emph{k} boys, \emph{k} girls, and \emph{2k} homes.
			\item Let the gadgets constructed be placed sequencially in a "circle", like the picture.
			\item Let the homes $h_{2i-1}$ represent occurences of \emph{x}.
			\item Let the homes $h_{2i}$ represent occurences of $\neg x$.
			\item If $(\# \neg x) \not = (\# x )$ then let those homes float around, unconnected.
			\item If a matching exists, then $b_i, g_i, h_{2i} \in E'$ OR $b_i, g_{i-1}, h_{2i-1} \in E'$, for $i = 1, \dots, k$ where $E'$ is the chosen solution.
			\item If we select the first choice, $T(x) = TRUE$. The other way, it results to \emph{FALSE}
		\end{enumerate}
	\item For each clause in f, create a boy and a girl. 
	\begin{enumerate}
		\item Each of these pairs will have 3 homes, resulting in their variables in that exact clause.
		\item If one of these 3 homes is unassigned, we can assign the boy and girl to that.
		\item The unassigned home corresponds to a TRUE literal, and the clause can be satisfied. 
		\item If there is no home available, resulting in the boy and the girl being homeless, we have a clause which is unsatisfiable.
	\end{enumerate}
	
\end{enumerate}
\begin{center}
	\includegraphics[width=0.5\textwidth]{tripartite2}
\end{center}

In the end, there will be some homes left. If there is \emph{m} clauses, there will be 3m homes.
We will construct boy and girl pairs and match these with the remaining, to finally have reduced the problem to tripartite matching.

\subsection{EXACT COVER BY 3-SETS}
\begin{description}
	\item[Instance] Universe X, $|X| = 3M$. Family $F=\{S_1, \dots, S_n \}$, all subsets of \emph{X}. $|S_i| = 3$
	\item[Question] Are there subsets $T_1 \cup T_2 \cup \dots \cup T_M = X?$
\end{description}

\subsection{TRIPARTITE MATCHING $\leq$ EXACT COVER BY 3-SETS}
To reduce to exact cover by 3-sets, we must know the following:
\begin{itemize}
	\item Sets $S_i = \{b, g, h \}$ will be represented as the possible matchings in our tripartite instance.
	\item Triples $t_i = (b,g,h)$ will describe a chosen solution. These must be disjunct sets for the exact cover to be satisfied.
	\item The union of boys, girls and homes can be described as $U = B \cup G \cup H$, and the size of this describes our universe, namely: $|U| = 3m$.
	\item The triples $t_i$ will be a subset of $T \subseteq B \times G \times H$.
	\item The final triples $T_1 \cup \dots \cup T_m$ will have length of $3m$.
\end{itemize}
If we can construct the cover, we have also constructed an appropriate matching in the tripartite instance.

\subsection{KNAPSACK}
\begin{description}
	\item[Instance] Items $i \in I$ where $i = (v_i, w_i)$. Limit \emph{W}, Goal \emph{K}.
	\item[Question] Find Set containing $\sum_{i \in S} w_i \leq W$ and $\sum_{i \in S} v_i \geq K$.
\end{description}

\subsection{EXACT COVER BY 3-SETS $\leq$ KNAPSACK}
Vi vil repræsentere Exact Cover med et knapsack problem.
Vi bruger en talbase i forhold til antallet af Set, så vi undgår carries.
\begin{center}
	\includegraphics[width=\textwidth]{knapsack}
\end{center}

\subsection{SET COVERING}
\begin{description}
	\item[Instance] Family $F=\{S_1, \dots, S_n \}$, all subsets of \emph{X}. Target \emph{B}.
	\item[Question] Are there subsets $T_1 \cup T_2 \cup \dots \cup T_B = X?$
\end{description}

\subsection{SET PACKING}
\begin{description}
	\item[Instance] Family $F=\{S_1, \dots, S_n \}$, all subsets of \emph{X}. Target \emph{K}.
	\item[Question] $T_1, \dots, T_K \in F \text{ subject to } 
					T_i \cap T_j = \emptyset \text{ for } i \not = j?$
\end{description}
 

\newpage
\section{Approximation algorithms}
We want near-optimal solutions, which can be found in polynomial time.
The symbol $\rho$ denotes an approximation factor. An approximation factor is defined as follows:
\begin{equation}
	max(\frac{C}{C^\star}, \frac{C^\star}{C}) \leq \rho (n)
\end{equation}
Where $C$ is the cost of the solution produced by an algorithm, and $C^\star$ is the optimal solution.

\subsection{Optimal Vertex-cover problem}
\begin{description}
	\item[Instance] Graph $G = (V,E)$
	\item[Question] Find the minimal subset $V' \subseteq V$ such that $\forall (u,v) \in E | u \in V' or v \in V'$ (or both)
\end{description}

\subsection{Approximation to Vertex-Cover}
\begin{center}
\begin{lstlisting}[mathescape, language=bash, numbers=left,basicstyle=\small, columns=fullflexible]
C $\leftarrow \emptyset$
E' $\leftarrow$ E[G]
while E' $\not = \emptyset$
	let (u,v) be an arbitrary edge of E'
	C $\leftarrow C \cup \{u,v\}$
	remove from E' every edge incident on either u or v
return C
\end{lstlisting}
\end{center}


\subsection{Vertex-Cover approximation algo is 2-approx}
We get some Vertex Cover from the algorithm,  since all edges have been covered.\\
When we pick an edge on line 4, the associated vertices' edges will also be deleted from E'.\\
Let \emph{A} denote the set of edges picked in line 4. \\
For a set to be a cover, each vertice must be included. No edges in A are covered by the same vertice in E'. The picked edges of A will have an endpoint in the set from $C^\star$.
Since we know that, we have a lower bound:
\begin{equation}
	|C^\star| \geq |A|
\end{equation}
Since the edges picked in A are a smaller set than the actual amount of edges covered in E', this works.
When we pick an edge in E', we ensure that we have covered two vertices. The amount of vertices covered are 
then half of the edges picked from A, resulting in an upper bound. 
\begin{equation}
	\begin{array}{lcl}
	|C| 	& 	= 	& 2 |A| \\
	 		& \leq 	& 2|C^ \star| 
	\end{array}
\end{equation}
This concludes that we have an 2-approx algorithm that runs in polynomial time!!

\subsection{The General Design/Analysis Trick}
Usually we compare our solution to some \textbf{lower bound} for minimization problems for the optimal solution.
For maximization problems it's \textbf{upper bounds}.
We sometimes create a solution with some \textbf{relaxation} to the problem, and then increase the cost to make it a feasible solution.

\subsection{Travelling Salesman Problem}
Given $n \times n$ positive distance matrix ($d_{ij}$),\\
Find permutation $\pi$ on $\{0,1,2, \dots, n-1\}$ minimizing $\sum\limits_{i=0}^{n-1} d_{\pi (i), \pi (i+1 \text{ mod } n)}$

\subsubsection{The Triangle Inequality}
The following states that for any triangle, the sum of the lengths for any two sides must be greate or equal to the length of the remaining side. 
\begin{equation}
	|x+y| \leq |x|+|y|
\end{equation}

\subsection{Approx-TSP-Tour}
Given graph G, and cost c do the following:
\begin{center}
\begin{lstlisting}[mathescape, numbers=left, basicstyle=\small, columns=fullflexible]
Select a vertex $r \in V[G]$ to be "root" vertex
Compute a minimum spanning tree T for G from root r
	using MST-PRIM(G,c,r) 
Let L be the list of vertices visited in a preorder tree walk of T
return the hamiltonian cycle H that visits the vertices in the order L
\end{lstlisting}
\end{center}

Running time for MST-PRIM is $\Theta (V^2)$\\
Do the example from the book, where the walks are lexicografically sorted.
\begin{center}
	\includegraphics[width=0.7\textwidth]{approxtsp1}
\end{center}
And the optimal solution:
\begin{center}
	\includegraphics[width=0.5\textwidth]{approxtsp2}
\end{center}

\subsection{Is it 2-approx?}
Hell yeah. \\
We can obtain a spanning tree by deleting a single edge from the optimal tour.\\
Let's call the optimal tour $H^\star$, and the minimum spanning tree we obtain \textit{T}.
\begin{equation}
	c(T) \leq c(H^\star)
\end{equation}
The above indicates a lower bound for the tour.\\

A \textbf{full walk} will traverse all edges of \textit{T} exactly twice.\\
Let's call the list returned by a full walk \textit{W}.\\
\begin{equation}
	c(W) = 2 \cdot c(T)
\end{equation}

Finally, this implies equation below.
\begin{equation}
	c(W) \leq 2 \cdot c(H^\star)
\end{equation}

Since W is not a tour yet, we need to do a final touch. \\ 
By the triangle inequality, we the cost will not increase if we delete a vertice from the list \textit{W}.
We do that until the list is distinct.\\
The ordering of \textit{W} is now the \textbf{same} as the one we obtained by doing the minimum spanning tree. \\
Let's call the preordered walk in the spanning tree \textit{H}.\\
This is a hamiltonian cycle, since every vertex is visited exactly once. Since \textit{H} is obtained by deleting vertices from the full walk \textit{W}, that implies
\begin{equation}
	c(H) \leq c(W)
\end{equation}
and we can finally conclude that 
\begin{equation}
	c(H) \leq 2\cdot c(H^\star)
\end{equation}


\end{document}